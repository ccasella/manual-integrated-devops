# Adding K8s cluster

GitLab offer a K8s integration that let you deploy easily your application in this environment.

A complete documentation regarding this can be found [here](https://docs.gitlab.com/ee/user/project/clusters/#adding-an-existing-kubernetes-cluster), following a quick step-by-step guide.

Select from the *Operation* menu the *Kubernetses* settings, a K8s cluter can be added at the instance, group or project level, we are going to use a project K8s cluster.

![Adding a K8s cluter](img/add-k8s-cluster.png)

If you select the GKE integration a new cluter will be created for you using your Google Account, if you choose to add an existing cluster the following parameters will be required:

**Kubernetes cluster name** - The name of your cluster  
**Environment scope** - You can choose if you want to use this cluster just for a specific environment  
**API URL** - The endpoint of K8s cluter API, you can get it from a working *kubectl* with this command:
```
kubectl cluster-info | grep master
``` 

To connect with your cluster you will need a *Service Account* with admin privileges, if you don't have it you can create it with:   
```
kubectl -n kube-system create serviceaccount gitlab
```

You will need also a *Cluster Role Binding*:  
```
kubectl create clusterrolebinding gitlab --clusterrole cluster-admin --serviceaccount=kube-system:gitlab
```

After this step you can continue with the following fields and relative commands (if you have a different *Service Account* use the correct naming):  
**CA Certificate** - The certificate for your connection
```
kubectl get secrets -n kube-system `kubectl get serviceaccount -n kube-system gitlab -o jsonpath={.secrets[0].name}` -o json | jq -r '.data["ca.crt"]' | base64 -D
```

**Service Token** - The token for your authentication
```
kubectl get secrets -n kube-system `kubectl get serviceaccount -n kube-system gitlab -o jsonpath={.secrets[0].name}` -o jsonpath={.data.token} | base64 -D
```

**Project namespace** - If you want you can set the *namespace* for the project, if not it will be self-generated.

As you can see from the [`.gitlab-ci.yml`](.gitlab-ci.yml) all we need to do to acces the cluster is define an [`environment`](https://docs.gitlab.com/ee/ci/environments.html). With the Kubernetes integration enabled for this environment we are provided the necessary CI variables for deploying using `kubectl`. The variables available in CI are described in full at https://docs.gitlab.com/ee/user/project/clusters/#deployment-variables .

With this integration we will be able to use [Prometheus monitoring](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html), [Deploy Boards](https://docs.gitlab.com/ee/user/project/deploy_boards.html), [Web terminals](https://docs.gitlab.com/ee/administration/integration/terminal.html) and access the [Kubernetes Pod logs](https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html#overview),  according to your license.

## Deploy base services

From the K8s integration, selecting the just configured cluster, it's possible to install many applications/services.

![K8s applications](img/k8s-applications.png)

Following the services that we are going to install:

**Helm Tiller** - This service is required to use Helm to deploy the other applications/services.  
**Ingress** - This service will expose a load balancer with a public ip address that will expose our services (cloud providers may charge for this), after the installation the public ip address will be returned in the dedicated field.  
**Cert-Manager** - This service will create for your exposed services an SSL certificate, a valid email is required.  
**Prometheus** - This service will take care of the monitoring of our applicatios and services.

## Domain setting

In the same screen you can setup the base domain for your application, if you have a valid external domain it's suggested that you forward a *STAR* record, like `*.mydomain.tld`.  
If you don't have a valid domain you can use, as suggested, the ip address and *nip.io*, like in this instance.

![Base Domain](img/domain-setting.png)

The suggestion will show the ip address of your *Ingress*.

## Test project

Now all requirements for this test project are ready.  
We will deploy a simple web application based on the NGINX docker image, to show the build stage the *index* page will be updated.

## Repository

Our project repo will contain the following files/directory

### Dockerfile

In our test application we are just going to change few lines in our index.html   

### index.html

Our new web page

### .gitlab-ci.yml

#### Build stage 

Here we have a build stage, for this project we use kaniko to avoid the requirement of privileged runners.

#### Deploy to Production

Here we have the deployment stage, we use a template for the kubernetes manifests and we generate the final manifests using environment variables.

### kubernetes folder

This folder will contain all the manifests templates required for the kubernetes deployment.

Some important notes for our manifests:

* **labels** format is used to match the pods in the monitoring on prometheus, changing this format will brake the graphs.
* **annotations** are used to match the pods in the environment screen, changing or omitting this will brake that screen.
* **replicas** variable is used to define the number of the instances for each deployment, you can use env scoped variable to set this for each env.

In the pipeline is inherited the *KUBE_INGRESS_BASE_DOMAIN* from our kubernetes configuration screen to set our *Ingress*.

Our project is ready!
